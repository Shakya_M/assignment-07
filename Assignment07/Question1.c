#include <stdio.h>

/* This programme will reverse a sentence entered by the user 
   The sentence_length < 1000 */

int main() {
    char string[1000];
    int index;
    // To get user input
    puts("Enter the sentence which you need to reverse :\n");
    gets(string);
    // To reverse
    for (index = strlen(string)-1; index >= 0; index--) {
        printf("%c", string[index]);
    }
    return 0;
} 

