# include <stdio.h>
/* This programme will find the frequency of a given character in a string entered by the user
   - All characters and strings are case sensitive
   - String_length <= 1000 */

int main() 
{
    char string[1000];
    char character;
    int index;
    int frequency = 0;
    // To get user input
    puts("Enter your string : ");
    gets(string);
    puts("Enter your character : ");
    character = getchar();
    // To search the frequency 
    for (index = 0; index < strlen(string); index++) {
        if (character == string[index]) {
            frequency++;
        }
    }
    printf("The Frequency of \"%c\" in \"%s\" is : %d", character, string, frequency);
    return 0;
}
