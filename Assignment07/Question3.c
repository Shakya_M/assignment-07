# include <stdio.h>
/* This programme will add and multiply two matices entered by the user */

int first[100][100], second[100][100], result[100][100];
int frows, srows, fcolumns, scolumns;
int x, y, z, user;
int total = 0;

int add_matrices();
int multiply_matrices();

// start
int main() {
    // decision
    printf("(1) Add matrices\n(2) Multiply matrices\n\n");
    scanf("%d", &user);
    if (user == 1) {
        add_matrices();
    } else if (user == 2) {
        multiply_matrices();
    } else {
        printf("Invalid input!");
    }
    return 0;
}

int add_matrices() {
    printf("\nEnter the dimensions (rows columns) of the first matrix :");
    scanf("%d %d", &frows, &fcolumns);
    puts("\nEnter the elements of first matrix => \n");
    // filling first matrix
    for (x = 0; x < frows; x++) {
        for (y = 0; y < fcolumns; y++) {
            scanf("%d", &first[x][y]);
        }
    }
    // second matrix
    printf("\nEnter the dimensions (rows columns) of the second matrix :");
    scanf("%d %d", &srows, &scolumns);
    // checking whether dimensions are equal or not
    if ((fcolumns != scolumns) || (frows != srows)) {
        printf("\nDimensions of both matrices must be equal!\n");
        goto end;
    }
    // filling second matrix
    puts("\nEnter the elements of second matrix => \n");
    for (x = 0; x < srows; x++) {
        for (y = 0; y < scolumns; y++) {
            scanf("%d", &second[x][y]);
        }
    }
    // result matrix
    for (x = 0; x < frows; x++) {
        for (y = 0; y < fcolumns; y++) {
            result[x][y] = first[x][y] + second[x][y];
        }
    }
    printf("\nResult =>\n\n");
    for (x = 0; x < frows; x++) {
        for (y = 0; y < fcolumns; y++) {
            printf("%d ", result[x][y]);
        }
        printf("\n");
    }
    end:
    return 0;
}

int multiply_matrices() {
    // first matrix
    printf("\nEnter the dimensions (rows columns) of the first matrix :");
    scanf("%d %d", &frows, &fcolumns);
    puts("\nEnter the elements of first matrix => \n");
    // filling first matrix
    for (x = 0; x < frows; x++) {
        for (y = 0; y < fcolumns; y++) {
            scanf("%d", &first[x][y]);
        }
    }
    // second matrix
    printf("\nEnter the dimensions (rows columns) of the second matrix :");
    scanf("%d %d", &srows, &scolumns);
    // checking whether fcolumns == srows or not
    if (fcolumns != srows) {
        printf("\nColumn count of first matrix and Row count of second matrix must be equal!\n");
        goto end;
    }
    // filling second matrix
    puts("\nEnter the elements of second matrix => \n");
    for (x = 0; x < srows; x++) {
        for (y = 0; y < scolumns; y++) {
            scanf("%d", &second[x][y]);
        }
    }
    // result matrix
    for (x = 0; x < frows; x++) {
        for (y = 0; y < scolumns; y++) {
            for (z = 0; z < srows; z++) {
                total += first[x][z] * second[z][y];
            }
            result[x][y] = total;
            total = 0;
        }
    }
    printf("\nResult =>\n\n");
    for (x = 0; x < frows; x++) {
        for (y = 0; y < scolumns; y++) {
            printf("%d ", result[x][y]);
        }
        printf("\n");
    }
    end:
    return 0;
}
